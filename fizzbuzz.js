#!/usr/bin/env node

var i, mod3, mod5, result;

for (i=1; i < 101; ++i) {

    mod3 = i % 3 === 0;
    mod5 = i % 5 === 0;
    result = i;

    if (mod3 && mod5) {
        result = 'fizzbuzz';
    } else {
        if (mod3) {
            result = 'fizz';
        }

        if (mod5) {
            result = 'buzz';
        }
    }

    console.log(result);
}
